# House Prices - Advanced Regression Techniques


## Описание
Это демонстрационный проект, описывающий решение задачи регресии цен на недвижимость, на основе kaggle-соревнования (https://www.kaggle.com/competitions/house-prices-advanced-regression-techniques). Здесь приведенны примеры разведывательного анализа и предварительной обработки данных, а также тренировки и выбора модели машинного обучения.
Проект выполнен в виде тетради jupyter, работающий с .csv данными представленными в папке ./data.

## Зависимости
Для запуска этого проекта, на локальной машине должны быть установленны:
* python >= 3.10.12
* jupyter notebook >= 6.4.8
* pandas >= 2.0.3
* numpy >= 1.24.3
* seaborn >= 0.12.2
* matplotlib >= 3.7.2
* sklearn >= 1.3.0

## Использование
`git clone https://gitlab.com/study_projects_mihailmirza98/HousePrices.git`
